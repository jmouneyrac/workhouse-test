<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles default project css + bootstrap -->
        <link rel="stylesheet" href="{{ asset(elixir('css/app.css')) }}">
        <link rel="stylesheet" href="{{ asset(elixir('css/starterkit.css')) }}">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    <a href="{{ url('/login') }}">Login</a>
                    <a href="{{ url('/register') }}">Register</a>
                </div>
            @endif

            <div class="content">

                <div class="title m-b-md">
                    @yield('title')
                </div>

                <div class="crumbtrail links">
                    <a href="{{ url('/') }}">Home</a>
                    <a href="{{ url('/estates') }}">Real Estates</a>
                    <a href="{{ url('/about') }}">About</a>
                </div>

                @yield('content')

            </div>
        </div>
    </body>
</html>
