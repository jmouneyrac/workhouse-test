@extends('layout')

@section('title')
    {{ $estate->name }}
@stop

@section('content')
    <div class="info">
        Price: {{ $estate->price }}
    </div>
    <div class="info">
        Bedrooms: {{ $estate->bedrooms }}
    </div>
    <div class="info">
        Bathrooms: {{ $estate->bathrooms }}
    </div>
    <div class="info">
        Storeys: {{ $estate->storeys }}
    </div>
    <div class="info">
        Garages: {{ $estate->garages }}
    </div>
@stop