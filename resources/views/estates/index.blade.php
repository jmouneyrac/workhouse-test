@extends('layout')

@section('title')
    Real Estates
@stop

@section('content')
    @foreach ($estates as $estate)
        <div class="estatetitle">
            <a href="{{ URL::to('/estates', $estate->id) }}">{{ $estate->name }}</a>
        </div>
    @endforeach
@stop