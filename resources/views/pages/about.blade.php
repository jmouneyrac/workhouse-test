@extends('layout')

@section('title')
   About
@stop

@section('content')
    <div class="info">
        This is a Real Estates app to demo Laravel.
    </div>
    <div class="info">
        It has been created for a test for a position at Workhouse Advertising.
    </div>
    <div class="info">
        © | copyright Jerome Mouneyrac
    </div>
@stop