<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Estate;

class EstatesController extends Controller
{
    // List of Real Estates.
    public function index() {
        $estates = Estate::all();
        return view('estates.index', compact('estates'));
    }

    public function show(Estate $estate) {
        return view('estates.show', compact('estate'));

    }
}
